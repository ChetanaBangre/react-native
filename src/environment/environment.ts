export const environment = {
    production: false,
    env: 'local',
    apiEndpoint: 'http://192.168.43.83:3000/',
    baseUrl: '/images/',
};
