export const Constants = {
    TOKEN_KEY: 'token',
    USER: 'user',
};

export const Messages = {
    UnknownError: 'An unknown error has occurred',
    SessionExpired: 'Your session has expired, please login again.',
};
