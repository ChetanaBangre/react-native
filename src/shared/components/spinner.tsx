import { ActivityIndicator, View, Modal } from 'react-native';
import { SpinnerStyles } from '../styles/';
import React from 'react';

export const Spinner = ({ animate }: { animate: boolean }) => {
    return (
        <Modal transparent={true} animationType={'none'}>
            <View style={SpinnerStyles.modalBackground}>
                <View style={SpinnerStyles.activityIndicatorWrapper}>
                    <ActivityIndicator animating={animate} color="#0000ff" />
                </View>
            </View>
        </Modal>
    );
};
