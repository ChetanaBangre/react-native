import { Text, TouchableOpacity } from 'react-native';

import { ButtonStyles } from '../styles/button-styles';
import React from 'react';

const Button = () => {
    return (
        <TouchableOpacity style={ButtonStyles.button}>
            <Text style={ButtonStyles.text}>Sign In</Text>
        </TouchableOpacity>
    );
};

export { Button };
