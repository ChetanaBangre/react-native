import { StyleSheet } from 'react-native';
import { ThemeStyles } from './';

const ButtonStyles = StyleSheet.create({
    button: {
        display: 'flex',
        height: 50,
        width: '50%',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: ThemeStyles.SECONDARY_BUTTON_COLOR,
        textAlign: 'center',
    },

    text: {
        fontSize: 16,
        textTransform: 'uppercase',
        color: '#FFFFFF',
    },
});

export { ButtonStyles };
