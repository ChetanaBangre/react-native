import { StyleSheet } from 'react-native';

const CommonStyles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
export { CommonStyles };
