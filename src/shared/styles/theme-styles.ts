export const ThemeStyles = {
    PRIMARY_BACKGROUND_COLOR: '#f5791f',
    PRIMARY_BUTTON_COLOR: '#fff',
    SECONDARY_BUTTON_COLOR: '#ccc',
};
