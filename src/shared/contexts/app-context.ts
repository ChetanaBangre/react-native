import React from 'react';

export type AppContextType = {
    showSnackbar: (message: string, isError: boolean) => void;
    setAuthenticated: (authenticated: boolean) => void;
};

const AppContext = React.createContext<AppContextType>({
    showSnackbar: (_message: string, _isError: boolean) => {},
    setAuthenticated: (_authenticated: boolean) => {},
});

export default AppContext;
