import { StyleSheet } from 'react-native';
import { ThemeStyles } from './../../shared/styles/theme-styles';

const WelcomeStyles = StyleSheet.create({
    welcomeContainer: {
        backgroundColor: ThemeStyles.PRIMARY_BACKGROUND_COLOR,
    },

    backgroundImage: {
        resizeMode: 'cover',
        width: '100%',
    },

    welcomeButton: {
        backgroundColor: 'white',
    },
});
export { WelcomeStyles };
