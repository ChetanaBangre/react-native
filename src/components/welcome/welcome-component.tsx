import { ButtonStyles, CommonStyles } from '../../shared/styles/';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';

import { Provider as PaperProvider } from 'react-native-paper';
import React from 'react';
import { WelcomeStyles } from '.';

interface WelcomeProps {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const WelcomeComponent = (props: WelcomeProps) => {
    return (
        <PaperProvider>
            <View style={[CommonStyles.container, WelcomeStyles.welcomeContainer]}>
                <Image source={require('../../assets/goavega-logo.png')} style={WelcomeStyles.backgroundImage} />
                <TouchableOpacity
                    style={[ButtonStyles.button, WelcomeStyles.welcomeButton]}
                    onPress={() => props.navigation.navigate('Login')}
                >
                    <Text>Welcome</Text>
                </TouchableOpacity>
            </View>
        </PaperProvider>
    );
};

export { WelcomeComponent };
