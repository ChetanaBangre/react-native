import { StyleSheet } from 'react-native';
import { ThemeStyles } from '../../shared/styles/theme-styles';

const LoginStyles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: ThemeStyles.PRIMARY_BACKGROUND_COLOR,
    },
    formView: {
        width: '60%',
    },
    formField: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        borderRadius: 4,
        marginBottom: 10,
    },
    header: {
        fontSize: 25,
        marginBottom: 20,
    },
    errorMessage: {
        fontSize: 10,
        color: 'red',
    },
});
export { LoginStyles };
