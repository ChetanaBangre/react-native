import AppContext, { AppContextType } from '../../shared/contexts/app-context';
import { ButtonStyles, CommonStyles } from '../../shared/styles';
import { NavigationParams, NavigationScreenProp } from 'react-navigation';
import React, { useContext } from 'react';
import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native';

import { AuthenticationRequest } from '../../models/requests';
import { Formik } from 'formik';
import { LoginService } from '../../services';
import { LoginStyles } from './';
import { LoginValidator } from '../../validations/login-validator';
import { Messages } from '../../shared/constant';
import { NavigationState } from '@react-navigation/native';
import { Provider as PaperProvider } from 'react-native-paper';
import { ResponseViewModel } from '../../models/responses';
import { TextInput } from 'react-native-paper';

interface LoginProps {
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const LoginComponent = (props: LoginProps) => {
    const context = useContext(AppContext);
    const handleLogin = async (request: AuthenticationRequest) => {
        const appContext: AppContextType = context;
        try {
            const authenticationResult = await LoginService.login(request);
            if (ResponseViewModel.hasErrors(authenticationResult)) {
                appContext.showSnackbar(ResponseViewModel.getError(authenticationResult), true);
                return;
            }
            appContext.setAuthenticated(true);
            appContext.showSnackbar('Logged in successfully', false);
            props.navigation.navigate('Home');
            // need to redirect now
        } catch (error) {
            appContext.showSnackbar(Messages.UnknownError, true);
        }
    };
    return (
        <PaperProvider>
            <SafeAreaView style={CommonStyles.container}>
                <Text style={LoginStyles.header}>Sign In</Text>
                <Formik
                    initialValues={{ email: '', password: '' }}
                    onSubmit={(values) => handleLogin(values)}
                    validationSchema={LoginValidator}
                >
                    {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
                        <View style={LoginStyles.formView}>
                            <TextInput
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                value={values.email}
                                placeholder="Email"
                                style={LoginStyles.formField}
                                label="Email"
                                keyboardType="email-address"
                            />
                            {touched.email && errors.email && (
                                <Text style={LoginStyles.errorMessage}>{errors.email}</Text>
                            )}
                            <TextInput
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                                placeholder="password"
                                style={LoginStyles.formField}
                                textContentType="password"
                                secureTextEntry
                            />
                            {touched.password && errors.password && (
                                <Text style={LoginStyles.errorMessage}>{errors.password}</Text>
                            )}
                            <TouchableOpacity
                                style={[LoginStyles.buttonContainer, ButtonStyles.button]}
                                onPress={handleSubmit}
                                activeOpacity={0.7}
                            >
                                <Text>Login</Text>
                            </TouchableOpacity>
                        </View>
                    )}
                </Formik>
            </SafeAreaView>
        </PaperProvider>
    );
};
export { LoginComponent };
