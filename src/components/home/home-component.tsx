import React from 'react';
import { Title } from 'react-native-paper';

const HomeComponent = () => {
    return <Title>Home Screen</Title>;
};
export { HomeComponent };
