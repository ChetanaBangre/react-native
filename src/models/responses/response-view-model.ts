import { ErrorResponse } from '.';
import { Messages } from '../../shared/constant';

export class ResponseViewModel<T> {
    public errors: ErrorResponse[] = [];
    public data: T | null = null;
    public static getErrors<T>(viewModel: ResponseViewModel<T>): string[] {
        if (viewModel && viewModel.errors) {
            return viewModel.errors.map((error) => error.message);
        }
        return [Messages.UnknownError];
    }

    public static getError<T>(viewModel: ResponseViewModel<T>): string {
        const errors = ResponseViewModel.getErrors(viewModel);
        return errors.length > 0 ? errors[0] : Messages.UnknownError;
    }

    public static hasErrors<T>(viewModel: ResponseViewModel<T>): boolean {
        return !viewModel || !viewModel.data || (viewModel.errors && viewModel.errors.length > 0);
    }

    public static getNullable<T>(request: ResponseViewModel<T>): ResponseViewModel<T> {
        return request ? request : new ResponseViewModel();
    }
}
