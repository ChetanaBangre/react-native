export class AuthenticationResponse {
    token? = '';
    id = '';
    role? = '';
    name? = '';
    email = '';
    password = '';
    createdAt = '';
}
