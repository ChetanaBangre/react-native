import { AsyncStorage } from 'react-native';

export class StorageService {
    public static storageProvider = AsyncStorage;

    public static setProvider(provider: any) {
        StorageService.storageProvider = provider;
    }

    public static async store<T>(key: string, value: T | null) {
        if (value === null || value === undefined) {
            StorageService.storageProvider.removeItem(key);
            return;
        }
        await StorageService.storageProvider.setItem(key, JSON.stringify(value));
    }

    public static async get<T>(key: string): Promise<T | null> {
        const value = await StorageService.storageProvider.getItem(key);
        if (!value) {
            return null;
        }
        return JSON.parse(value) as T;
    }

    public static async removeKey(key: string) {
        await StorageService.storageProvider.removeItem(key);
    }
}
