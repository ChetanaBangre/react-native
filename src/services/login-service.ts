import { HttpBaseService, StorageService } from '.';

import { AuthenticationRequest } from '../models/requests';
import { AuthenticationResponse } from '../models/responses';
import { Constants } from '../shared/constant';
import { ResponseViewModel } from '../models/responses';

export class LoginService {
    private static storeToken(authenticationResponse: AuthenticationResponse) {
        StorageService.store(Constants.TOKEN_KEY, authenticationResponse.token);
        StorageService.store(Constants.USER, authenticationResponse);
    }

    public static async getLoggedInUser(): Promise<AuthenticationResponse | null> {
        return await StorageService.get<AuthenticationResponse>(Constants.USER);
    }

    public static async login(loginRequest: AuthenticationRequest): Promise<ResponseViewModel<AuthenticationResponse>> {
        const response = await HttpBaseService.post<AuthenticationRequest, AuthenticationResponse>(
            'login',
            loginRequest,
        );
        if (response && response.data) {
            this.storeToken(response.data);
        }
        return response;
    }
}
