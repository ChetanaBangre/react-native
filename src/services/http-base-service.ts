import { Constants } from '../shared/constant';
import { ResponseViewModel } from '../models/responses/response-view-model';
import { StorageService } from '.';
import axios from 'axios';
import { environment } from '../environment/environment';

interface AxiosErrorResponse {
    status: number;
}
interface AxiosError {
    response: AxiosErrorResponse;
}
export class HttpBaseService {
    public static init(errorCallback: (statusCode: number) => void, loadingCallback: (really: boolean) => void) {
        axios.defaults.baseURL = environment.apiEndpoint;
        // axios.defaults.baseURL = 'http://localhost:3000';
        axios.defaults.headers = {
            'content-type': 'application/json',
        };
        axios.interceptors.request.use(
            async (config) => {
                const token = await StorageService.get<string>(Constants.TOKEN_KEY);
                if (token) {
                    config.headers = {
                        ...config.headers,
                        Authorization: `jwt ${token}`,
                    };
                }
                loadingCallback(true);
                return config;
            },
            (error: any) => {
                loadingCallback(false);
                return Promise.reject(error);
            },
        );
        axios.interceptors.response.use(
            (response) => {
                loadingCallback(false);
                return response;
            },
            (error: AxiosError) => {
                loadingCallback(false);
                const errorPromise = Promise.reject(error);
                if (error.response) {
                    errorCallback(error.response.status);
                }
                return errorPromise;
            },
        );
    }

    public static async get<T>(url: string): Promise<ResponseViewModel<T>> {
        try {
            const response = await axios.get<ResponseViewModel<T>>(url);
            return response.data;
        } catch (e) {
            return Promise.reject(e);
        }
    }

    public static async post<Req, Res>(url: string, body: Req) {
        try {
            const response = await axios.post<ResponseViewModel<Res>>(url, body);
            return response.data;
        } catch (e) {
            return Promise.reject(e);
        }
    }

    public static async put<Req, Res>(url: string, body: Req) {
        try {
            const response = await axios.put<ResponseViewModel<Res>>(url, body);
            return response.data;
        } catch (e) {
            return Promise.reject(e);
        }
    }

    public static async upload<Res>(url: string, body: FormData) {
        try {
            const response = await axios.post<ResponseViewModel<Res>>(url, body);
            return response.data;
        } catch (e) {
            return Promise.reject(e);
        }
    }

    public static async export<Blob>(url: string) {
        try {
            const response = await axios.post<Blob>(url, null, { responseType: 'blob' });
            return response;
        } catch (e) {
            return Promise.reject(e);
        }
    }
}
