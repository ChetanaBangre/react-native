import * as Yup from 'yup';

export const LoginValidator = Yup.object()
    .shape({
        email: Yup.string().trim().email('Please enter a valid email id').label('Email').required(),
        password: Yup.string().trim().min(5).label('Password').required(),
    })
    .required();
