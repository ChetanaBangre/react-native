# README #

# run command to install packages
yarn install

# run react native
react-native start

# run android
react-native run android

# run ios
react-native run ios

# run command to run mock server (change to your system IP address)
json-server db.json --middlewares middleware.js --host 192.168.43.83

# for login form
any email and password will work