/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler';

import React, { useState } from 'react';

import AppContext from './src/shared/contexts/app-context';
import { HomeComponent } from './src/components/home/home-component';
import { HttpBaseService } from './src/services/http-base-service';
import { LoginComponent } from './src/components/login';
import { LoginService } from './src/services';
import { NavigationContainer } from '@react-navigation/native';
import { Snackbar } from 'react-native-paper';
import { Spinner } from './src/shared/components/spinner';
import { WelcomeComponent } from './src/components/welcome';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();
const App = () => {
    HttpBaseService.init(
        (statusCode) => {
            // you would want to handle 401 differently than others
            console.warn('received error with status', statusCode);
        },
        (really) => {
            // really tells us if we need to show or hide the spinner
            setLoader(really);
        },
    );
    const [showSnack, setShowSnack] = useState(false);
    const [showLoader, setLoader] = useState(false);
    const [snackMessage, setSnackMessage] = useState('');
    const [isAuthenticated, setIsAuthenticated] = useState(!LoginService.getLoggedInUser());
    const onShowSnackbar = (message: string, isError: boolean): void => {
        setShowSnack(isError);
        setSnackMessage(message);
        setShowSnack(!showSnack);
        console.warn('showsnack', showSnack, message);
    };
    const setAuthenticated = (authenticated: boolean): void => {
        setIsAuthenticated(authenticated);
        console.warn('authenticated', isAuthenticated);
    };

    const onDismissSnackBar = (): void => {
        setShowSnack(false);
    };

    return (
        <AppContext.Provider value={{ showSnackbar: onShowSnackbar, setAuthenticated: setAuthenticated }}>
            {showLoader ? <Spinner animate={showLoader} /> : null}
            {showSnack ? (
                <Snackbar
                    visible={showSnack}
                    onDismiss={onDismissSnackBar}
                    action={{
                        label: 'Undo',
                        onPress: () => {
                            // Do something
                        },
                    }}
                >
                    {snackMessage}
                </Snackbar>
            ) : null}
            <NavigationContainer>
                <Stack.Navigator
                    initialRouteName="Welcome"
                    screenOptions={{
                        headerShown: false,
                    }}
                >
                    <Stack.Screen name="Welcome" component={WelcomeComponent} />
                    <Stack.Screen name="Login" component={LoginComponent} />
                    <Stack.Screen name="Home" component={HomeComponent} />
                </Stack.Navigator>
            </NavigationContainer>
        </AppContext.Provider>
    );
};

export default App;
